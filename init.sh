#!/bin/bash

cd ~
sudo apt update && sudo apt install -y virtualenv git

git config --global user.name "Flavio Silva"
git config --global user.email "flavio.asilva@gmail.com"

git clone https://gitlab.com/flavioasilva2/autoconf && cd autoconf

virtualenv .venv
source .venv/bin/activate
pip install -U pip
pip install -r requirements.txt
