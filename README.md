# For the playbook execution
Run ```~/init.sh``` from linux template,

then activate python venv from autoconf directory

then:

```ansible-playbook -K site.yml```

# Docs:

[Ansible](https://docs.ansible.com/)

[Virtualenv](https://virtualenv.pypa.io/en/latest/)
